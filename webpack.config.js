var path = require('path');

module.exports = {
  entry: {
    client: './src/client.js',
    server: './src/server.js',
    rapp1: './test/react/test1/app.js',
    f1: './test/html/frame1.js',
    f2: './test/html/frame2.js',
    f3: './test/html/frame3.js'
  },
  output: {
    filename: 'esjs-[name].js',
    path: path.resolve(__dirname, 'dist')
  }
};