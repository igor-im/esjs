import * as _ from 'lodash-es'
import * as utils from './utils'

import { isBrowser, isNode } from './utils'

// create event store
export default class eventStore {
    constructor() {
        console.log('event store init')
        utils.setMagicNumber()
        this.consumers = {}
        this.producers = {}
        this.events = {}
        utils.globalListen()
    }
    dispatchEvent(evt) {
        utils.dispatch(this.events[evt])
    }
    registerConsumer(consumer, evts, options) {
        console.log('register store consumer')
        let uuid = utils.generateUUID()
        this.consumers[consumer.id] = consumer
        this.consumers[consumer.id].uuid = uuid
        evts.map((v,i) => {
            let evtId = this.registerEvent(v)
            this.events[evtId].registerConsumer(uuid)
        })
        return uuid
    }
    registerProducer(producer, evts, options) {
        console.log('register store producer')
        let uuid = utils.generateUUID()
        this.producers[producer.id] = producer
        this.producers[producer.id].uuid = uuid
        evts.map((v,i) => {
            let evtId = this.registerEvent(v)
            this.events[evtId].registerProducer(uuid)
            utils.listen(this.events[evtId])
        })
        return uuid;
    }
    registerEvent(evt) {
        if(this.events[evt.id])
            console.log('exists')
        else
            this.events[evt.id] = new ESEvent(evt)
            return evt.id
    }
    currentStore() {
        console.log(this)
    }
}

export class ESEvent {
    constructor(evt) {
        console.log('create event')
        this.consumers = []
        this.producers = []
        this.type = evt.type
        this.events = [{type: 'vehicle_change', payload: { test: 'testdata'}}]
    }
    registerConsumer(consumer, options) {
        this.consumers.push(consumer)
        console.log('register event consumer')
    }
    registerProducer(producer, options) {
        this.consumers.push(producer)
        console.log('register event producer')
    }
    getLatest() {
        return this.events[this.events.length - 1]
    }
}



var a = new eventStore()
a.registerConsumer({id: 1}, [{id: 1, type:'vehicle_change'}, {id: 2, type:'vehicle_booked'}])
a.registerProducer({id: 1}, [{id: 1, type:'vehicle_change'}, {id: 2, type:'vehicle_booked'}])
var e = a.registerEvent({id: 3, type:'vehicle_added'})
a.events[e].registerConsumer('aaa')
//a.dispatchEvent('1')
//a.currentStore()