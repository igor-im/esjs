import EventEmitter from 'events'

export function generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

export function getAddress(id, options) {
    switch(options.type) {
        case 'iframe':
            return document.getElementsByName(id)[0].contentWindow
        case 'samewindow':
            return options.handler
        default: 
            throw new Error('Can\'t get address')
    }
}

export function setMagicNumber() {
    let UUID = generateUUID();
    if(isNode) {
        if(global.EventStoreUUID) {
            throw new Error('There can only be 1 instance of the Event Store')
        }
        global.EventStoreUUID = UUID
    }
    if(isBrowser) {
        if(window.EventStoreUUID) {
            throw new Error('There can only be 1 instance of the Event Store')
        }
        window.EventStoreUUID = UUID
    }
    console.log('UUID set to',UUID)
}

export function dispatch(evt, address) {
    //let latest = evt.getLatest()
    if(isNode) {
        emitter.emit(latest.type, latest)
    }
    if(isBrowser) {
        if(typeof address === 'function') {
            address(evt)
            return null;
        }
        address.postMessage(evt, '*')
    }
    console.log('event dispatched')
}

export function listen(evt) {
    if(isNode) {
        emitter.on(evt.type, (evt) => {
            console.log('got message', evt)
        })
    }
    if(isBrowser) {
        window.addEventListener(evt.type, (evt) => {
            console.log('got message', evt)
        })
    }
}

export function globalListen(command) {
    if(isNode) {
        process.on('message', (event) => {
            command(event.data)
        })
    }
    if(isBrowser) {
        window.onmessage = (event) => {
            command(event.data)
        }
    }
}

export const isBrowser=(new Function("try {return this===window;}catch(e){ return false;}"))();
export const isNode=(new Function("try {return this===global;}catch(e){return false;}"))();
const emitter = new EventEmitter()