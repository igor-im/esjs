import { isBrowser, isNode } from './utils'

export default class ESClient {
    constructor(events, handler, name) {
        this.type = (() => {
            if(self==top) {
                return 'samewindow'
            } else {
                return 'iframe'
            }
        })()
        this.id = (() => {
            if(this.type === 'iframe') 
                return window.name
            else if(this.type === 'samewindow')
                return name
        })()
        if(this.type === 'samewindow') {
            if(!handler)
                throw new Error('samewindow client must provide message handler')
            this.handler = handler
        } else if(this.type === 'iframe') {
            this.handler = handler ? handler : function(){}
        }
        this.listeners = events.listen
        this.producers = events.produce
        this.registerWithServer()
    }

    registerWithServer() {
        if(this.type == 'samewindow') {
            window.EventStore.route({
                type: 'INIT',
                listen: this.listeners,
                publish: this.producers,
                identity: {
                    id: this.id,
                    type: 'samewindow',
                    handler: this.handler
                }
            }, '*')
        } else if(this.type == 'iframe') {
            parent.postMessage({
                type: 'INIT',
                listen: this.listeners,
                publish: this.producers,
                identity: {
                    id: window.name,
                    type: 'iframe'
                }
            }, '*')
            window.addEventListener('message', (evt) => {
                console.log(this.id,evt)
                this.handler(evt)
            })
        }
    }

    unregisterFromServer() {

    }

    sendMessage(evt) {
        if(this.type == 'samewindow') {
            window.EventStore.route(evt)
        } else if(this.type == 'iframe') {
            parent.postMessage(evt, '*')
        }
    }
}