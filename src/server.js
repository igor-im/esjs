import * as _ from 'lodash-es'
import * as utils from './utils'

import { isBrowser, isNode } from './utils'

console.log('main loaded')

// var frame1 = document.getElementsByName('frame1')[0]
// var frame1Content = frame1.contentWindow
// var frame2 = document.getElementsByName('frame2')[0]
// var frame2Content = frame2.contentWindow

// document.addEventListener('click', () => {
//     frame1Content.postMessage('Hello Frame 1', '*')
//     frame2Content.postMessage('Hello Frame 2', '*')
// })

export default class EventStore {
    constructor() {
        console.log('event store init')
        utils.setMagicNumber()
        this.consumers = {}
        this.producers = {}
        this.events = {}
        this.init()
        this.eventLog = []
    }
    init() {
        utils.globalListen(this.route.bind(this))
    }
    dispatchEvent(evt) {
        utils.dispatch(this.events[evt])
    }
    processEvent(evt) {
        if(!evt.type)
            return null
        this.eventLog.push(evt)
        let event = this.events[evt.type]
        event.processEvent(evt)
    }
    registerConsumer(consumerID, evts, options) {
        console.log('register store consumer')
        let consumer = { 
            address: utils.getAddress(consumerID, options),
            id: consumerID,
            uuid: utils.generateUUID()
        }
        this.consumers[consumer.id] = consumer
        evts.map((v,i) => {
            let evtId = this.registerEvent(v)
            this.events[evtId].registerConsumer(consumer)
        })
        return consumer.uuid
    }
    registerProducer(producerID, evts, options) {
        console.log('register store producer')
        let producer = { 
            address: utils.getAddress(producerID, options),
            id: producerID,
            uuid: utils.generateUUID()
        }
        this.producers[producer.id] = producer
        evts.map((v,i) => {
            let evtId = this.registerEvent(v)
            this.events[evtId].registerProducer(producer)
            utils.listen(this.events[evtId])
        })
        return producer.uuid;
    }
    registerEvent(evt) {
        if(this.events[evt.id])
            console.log('exists')
        else
            this.events[evt.id] = new ESEvent(evt)
            return evt.id
    }
    route(event) {
        switch (event.type) {
            case 'INIT':
                console.log('init hit')
                this.registerConsumer(event.identity.id, event.listen, {type: event.identity.type, handler: event.identity.handler ? event.identity.handler : null})
                this.registerProducer(event.identity.id, event.publish, {type: event.identity.type, handler: event.identity.handler ? event.identity.handler : null})
                break;
            default:
                //this.dispatchEvent(event)
                this.processEvent(event)
        }
    }
    currentStore() {
        console.log(this)
    }
}

export class ESEvent {
    constructor(evt) {
        console.log('create event')
        this.consumers = []
        this.producers = []
        this.type = evt.type
        this.events = [/*{type: 'vehicle_change', payload: { test: 'testdata'}}*/]
    }
    registerConsumer(consumer, options) {
        this.consumers.push(consumer)
        console.log('register event consumer')
    }
    registerProducer(producer, options) {
        this.producers.push(producer)
        console.log('register event producer')
    }
    processEvent(evt, producer) {
        console.log('add evt', evt, 'from ', evt.producer)
        this.events.push(evt)
        this.consumers.map((v,i) => {
            utils.dispatch(evt, v.address)
            //v.address.postMessage(evt, '*')
        })
    }
    getLatest() {
        return this.events[this.events.length - 1]
    }
}

const eventStore = new EventStore()
window.EventStore = eventStore

window.setTimeout(() => {
    console.log('ready')
    document.addEventListener('click', () => {
        console.log(eventStore.currentStore())
    })
    var socket = new WebSocket("ws://localhost:8081");
    socket.addEventListener('open', function (event) {
        socket.send('Hello Server!');
    });

    // Listen for messages
    socket.addEventListener('message', function (event) {
        eventStore.processEvent(JSON.parse(event.data), 'websocket')
    });
}, 1000);