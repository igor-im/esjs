import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
// rollup.config.js
import { rollup } from 'rollup';

export default {
  entry: './src/index.js',
  dest: './dist/esjs.js',
  moduleName: 'EventSource',
  format: 'iife',
  plugins: [
    resolve(),
    commonjs()
  ]
};