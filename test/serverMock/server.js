const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8081 });

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    ws.send(JSON.stringify({type: 'VEHICLE_CHANGE', payload: { year: 2017, make: 'honda', model: 'civic', trim: 'ex-l' }}));
  });
});