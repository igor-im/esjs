import ESClient from '../../../src/client.js'
import React from 'react';
import ReactDOM from 'react-dom';

let events = {
    listen: [{id: 'VEHICLE_CHANGE', type: 'VEHICLE_CHANGE'}],
    produce: [{id: 'VEHICLE_UPDATE', type: 'VEHICLE_UPDATE'}],
}

class Hello extends React.Component {
  constructor() {
    super()
    this.state = {
      messages: []
    }
    this.client = new ESClient(events, 
      (e) => { this.setState({ messages: this.state.messages.concat([JSON.stringify(e.payload)]) }) }, 
      'div1')
  }
  render() {
    return React.createElement('div', 
      null, 
      [`Hello from Div1`, 
      this.state.messages.map((v)=> {return React.createElement('div', null, [v])}),
      React.createElement('button', {
            onClick: () => { this.client.sendMessage({type: 'VEHICLE_UPDATE', payload: 'hello from div 1'}) }
          }, ['Send VEHICLE_UPDATE'])]);
    }
}

ReactDOM.render(
  React.createElement(Hello),
  document.getElementById('div1')
);