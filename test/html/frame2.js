import ESClient from '../../src/client.js'

let events = {
    listen: [{id: 'VEHICLE_UPDATE', type: 'VEHICLE_UPDATE'}],
    produce: [{id: 'VEHICLE_CHANGE', type: 'VEHICLE_CHANGE'}],
}

function addResult(evt) {
    let el = document.createElement('div')
    el.innerText = JSON.stringify(evt.data.payload)
    document.getElementById('results').appendChild(el)
}

let client = new ESClient(events, addResult)

console.log('f2 loaded')

document.getElementById('button1').addEventListener('click', (e) => {
    client.sendMessage({type: 'VEHICLE_CHANGE', payload: 'hello from frame 2'})
})