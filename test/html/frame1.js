import ESClient from '../../src/client.js'

let events = {
    listen: [{id: 'VEHICLE_CHANGE', type: 'VEHICLE_CHANGE'}],
    produce: [{id: 'VEHICLE_UPDATE', type: 'VEHICLE_UPDATE'}],
}

function addResult(evt) {
    let el = document.createElement('div')
    el.innerText = JSON.stringify(evt.data.payload)
    document.getElementById('results').appendChild(el)
}

let client = new ESClient(events, addResult)

console.log('f1 loaded')

document.getElementById('button1').addEventListener('click', (e) => {
    client.sendMessage({type: 'VEHICLE_UPDATE', payload: 'hello from frame 1'})
})