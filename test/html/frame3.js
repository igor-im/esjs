import ESClient from '../../src/client.js'

let events = {
    listen: [{id: 'SOME_RANDOM_EVENT', type: 'SOME_RANDOM_EVENT'}],
    produce: [{id: 'VEHICLE_CHANGE', type: 'VEHICLE_CHANGE'}],
}

let client = new ESClient(events)

console.log('f3 loaded')